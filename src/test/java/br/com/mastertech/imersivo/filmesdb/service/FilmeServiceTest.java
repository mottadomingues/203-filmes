package br.com.mastertech.imersivo.filmesdb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;

@RunWith(SpringRunner.class)
//@SpringBootTest //Mas roda o spring inteiro
@ContextConfiguration(classes = {FilmeService.class})
public class FilmeServiceTest {
	@Autowired
	FilmeService filmeService;

	@MockBean
	FilmeRepository filmeRepository;
	
	@Test
	public void deveListarTodosOsFilmes()
	{
		//setup
		Long id = 1L;
		Filme filme = criarFilme(id);
		List<Filme> filmes = Lists.list(filme); //Só é possível utilizar Lists em testes unitários
		
		when(filmeRepository.findAll()).thenReturn(filmes);
		
		//action
		
			List<Filme> resultado = Lists.newArrayList(filmeService.obterFilmes());
			
			//ou
		
			//Iterable<Filme> resultado = filmeService.obterFilmes();
			//Iterator<Filme> iterador = resultado.iterator();
			//Filme filmeResultado = iterador.next();
			
			//ou
					
			//List<Filme> resultado = (List) filmeService.obterFilmes();
			//nesse caso podemos colocar o SuprressWarnings para o Sonar não capturar o alerta de possível erro de cast
		
		
		//check		
		assertNotNull(resultado.get(0));
		assertEquals(filme, resultado.get(0));
	}
	
	@Test
	public void deveListarTodosOsFilmesDeUmDiretor()
	{
		//setup
		long id = 1;

		FilmeDTO filmeDTO = mock(FilmeDTO.class);
		List<FilmeDTO> filmes = Lists.list(filmeDTO);
		
		when(filmeRepository.findAllByDiretor_Id(id)).thenReturn(filmes);
		
		//action
		List<FilmeDTO> resultado = Lists.newArrayList(filmeService.obterFilmes(id));
		
		//check
		assertEquals(filmeDTO, resultado.get(0));
		
	}
	
	
	@Test
	public void deveCriarUmFilme()
	{
		//setup
		Long id = 1L;
		Filme filme = criarFilme(id);
		
		//action
		filmeService.criarFilme(filme);
		
		//check
		Mockito.verify(filmeRepository).save(filme);
	}
	
	@Test
	public void deveEditarUmFilme()
	{
		//setup
		Long id = 1L;
		String novoGenero = "Comédia";
		String novoTitulo = "Bob";
		
		Filme filmeDoBanco = criarFilme(id);
		Filme filmeDoFront = criarFilme(id);
		filmeDoFront.setGenero(novoGenero);
		filmeDoFront.setTitulo(novoTitulo);
		
		when(filmeRepository.findById(filmeDoBanco.getId())).thenReturn(Optional.of(filmeDoBanco));
		when(filmeRepository.save(filmeDoBanco)).thenReturn(filmeDoBanco);
		
		//action
		Filme filmeResultado = filmeService.editarFilme(filmeDoBanco.getId(), filmeDoFront);
		
		
		//check
		assertNotNull(filmeResultado);
		assertEquals(novoGenero, filmeResultado.getGenero());
		assertNotEquals(novoTitulo, filmeResultado.getTitulo());
	}
	

	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOFilme()
	{
		//setup
		Long id = 1L;
		Long novoId = 10L;
		Filme filmeDoFront = criarFilme(id);
		filmeDoFront.setId(novoId);
		
		when(filmeRepository.findById(id)).thenReturn(Optional.empty());
		
		//action
		filmeService.editarFilme(id, filmeDoFront);
	}
	
	@Test
	public void deveApagarUmFilme()
	{
		//setup
		Long id = 1L;
		Filme filme = criarFilme(id);
		when(filmeRepository.findById(filme.getId())).thenReturn(Optional.of(filme));
		
		//action
		filmeService.apagarFilme(id);
		
		//check
		Mockito.verify(filmeRepository).delete(filme);
	}
	
	private Filme criarFilme(long id)
	{
		Filme filme = new Filme();
		filme.setId(id);
		filme.setGenero("Ação");
		filme.setTitulo("Matrix");
		
		return filme;
	}
	
	
}
