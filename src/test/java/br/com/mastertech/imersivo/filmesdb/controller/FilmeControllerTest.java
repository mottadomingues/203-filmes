package br.com.mastertech.imersivo.filmesdb.controller;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.service.FilmeService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {FilmeController.class})
public class FilmeControllerTest
{
	@MockBean
	FilmeService filmeService;
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void deveObterListaDeFilmes() throws Exception
	{
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setTitulo("Matrix");
		
		List<Filme> filmes = Lists.newArrayList(filme);
		
		Mockito.when(filmeService.obterFilmes()).thenReturn(filmes);
		
		mockMvc.perform
			(
				MockMvcRequestBuilders.get("/filme")
			)
			.andExpect
			(
				MockMvcResultMatchers.status().isOk()
			)
			.andExpect
			(
				MockMvcResultMatchers.content().string(CoreMatchers.containsString("Matrix"))
			);
	}
	
	@Test
	public void deveCriarUmFilme() throws JsonProcessingException, Exception
	{
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setTitulo("Matrix");
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		
		mockMvc.perform
		(
			post("/filme")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(filme))
		)
		.andExpect
		(
			status().isCreated()
		);
		
		verify(filmeService).criarFilme(Mockito.any(Filme.class));
	}
	
	@Test
	public void deveApagarFilme() throws Exception
	{
		long id = 1L;
		Filme filme = new Filme();
		filme.setId(id);
		
		Mockito.when(filmeService.encontraOuDaErro(id)).thenReturn(filme);
		
		mockMvc.perform
		(
			MockMvcRequestBuilders.delete("/filme/" + id)
		)
		.andExpect
		(
			status().isNoContent()
		);
	}
}
