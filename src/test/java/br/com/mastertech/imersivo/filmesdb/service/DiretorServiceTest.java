package br.com.mastertech.imersivo.filmesdb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Diretor;
import br.com.mastertech.imersivo.filmesdb.model.DiretorDTO;
import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.DiretorRepository;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DiretorService.class})
public class DiretorServiceTest
{

	@Autowired
	DiretorService diretorService;
	
	@MockBean
	FilmeService filmeService;
	
	@MockBean
	DiretorRepository diretorRepository;
	
	
	//setup
	long defaulId = 1L;
	Diretor diretor;
	
	@Before
	public void criarDiretor()
	{
		diretor = new Diretor();
		diretor.setId(this.defaulId);
	}
	
	@Test
	public void deveListarTodosDiretores()
	{
		//setup
		List<Diretor> diretores = Lists.list(diretor);
		
		when(diretorRepository.findAll()).thenReturn(diretores);
		
		//action
		List<Diretor> resultado = Lists.newArrayList(diretorService.obterDiretores());
		
		//check		
		assertNotNull(resultado.get(0));
		assertEquals(diretor, resultado.get(0));
	}
	
	@Test
	public void deveCriarUmDiretor()
	{
		//action
		diretorService.criarDiretor(diretor);
		
		verify(diretorRepository).save(diretor);		
	}
	
	@Test
	public void deveApagarUmDiretor()
	{
		//setup
		when(diretorRepository.findById(this.defaulId)).thenReturn(Optional.of(this.diretor));
		
		//action
		diretorService.apagarDiretor(this.defaulId);
		
		//check
		verify(diretorRepository).delete(this.diretor);
	}
	
	@Test(expected = RuntimeException.class)
	public void deveLancarExcecaoPorNaoEncontrarUmDiretorPeloId()
	{
		//setup
		when(diretorRepository.findById(this.defaulId)).thenReturn(Optional.empty());
		
		//action
		diretorService.apagarDiretor(this.defaulId);
	}
	
	@Test
	public void deveRetornarUmDiretor()
	{
		//setup
		DiretorDTO diretorDTO = mock(DiretorDTO.class);
		when(diretorRepository.findSummary(this.defaulId)).thenReturn(diretorDTO);
		
		//action
		diretorService.obterDiretor(this.defaulId);
	}
	
	@Test
	public void deveRetornarFilmes()
	{
		//setup
		FilmeDTO filmeDTO = mock(FilmeDTO.class);
		Iterable<FilmeDTO> filmes = Lists.list(filmeDTO);
		when(filmeService.obterFilmes(this.defaulId)).thenReturn(filmes);
		
		//action
		List<FilmeDTO> resultado = Lists.newArrayList(diretorService.obterFilmes(this.defaulId));
		
		//check
		assertNotNull(resultado.get(0));
		assertEquals(filmeDTO, resultado.get(0));		
	}
}
