package br.com.mastertech.imersivo.filmesdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmesdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilmesdbApplication.class, args);
	}

}
